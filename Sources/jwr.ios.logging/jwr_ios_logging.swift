//
//  jwr_ios_logging.swift
//  JWR iOS Library
//
//  Created by Jens Willy Johannsen on 15/06/2021.
//

import Foundation
import os

// MARK: - LogCategory

public struct LogCategory: Equatable {
	/// Default log category with an empty identifier
	public static let `default` = LogCategory( identifier: "" )

	public let identifier: String
	public let logger: Logger

	/**
	Initialize a logger with the specified category and the main bundle as subsystem.

	- parameter identifier: Category identifier for the logger
	*/
	public init(identifier: String) {
		self.identifier = identifier
		self.logger = Logger.init(subsystem: Bundle.main.bundleIdentifier!, category: identifier)
	}

	/**
	 `Equatable` conformance. The category´s `identifier` is used to check equality.
	*/
	public static func == (lhs: LogCategory, rhs: LogCategory) -> Bool {
		lhs.identifier == rhs.identifier
	}
}

// MARK: - Log

/**
Logging utiliy class

Use the `log()` and `trace()` functions to log messages. Utility functions `.info()`, `.debug()`, `.error()` and `.fault()` can also be used to log with the corresponding log level.

Note that the log level `.debug` is _not_ persisted and that `.info` is only persisted if collected using the `log` command line tool.
Also, the remaining log levels are only persisted _up to a storage limit_.

## Log Categories

Recommended practise is to extend `LogCategory` with new static properties for individual categories as follows:

```
extension LogCategory {
static let database = LogCategory( identifier: "🗄 DB" )
}
```

This will make it easy to filter the logs/console output for specific categories.

## Privacy

`Log.Privacy` specified two levels: `.public` and `.private`. The former uses `OSLogPrivacy.public` for logging and the latter uses `OSLogPrivacy.private( mask: .hash )`. Note that the privacy modifier is applied to the _entire_ log message and not just some interpolated content.

If it is required to use more specific `OSLogPrivacy` settings on logged content, then you must use the category's `Logger` directly:

```
Log[ .database ].error( "User with email \(email, privacy: .sensitive) failed to authenticate" )
```

Note that messages logged directly with a `Logger` is _not_ passed on to any other registered log handler.

*/
public class Log {
	/// Log privacy settings mirroring a simplified version of `OSLogPrivacy`
	public enum Privacy {
		/// Matching `OSLogPrivacy.public`
		case `public`

		/// Matching `OSLogPrivacy.private( mask: .hash )`
		case `private`
	}

	/**
	Type alias for a _log handler_ which is a closure that will be called on every log statement.

	- parameter message: Message string (does not include the `Log.prefix`)
	- parameter category: Log category identifier
	- parameter logLevel: Log level
	- parameter privacy: `Log.Privacy` level
	*/
	public typealias LogHandler = (_ message: String, _ category: String, _ logLevel: OSLogType, _ privacy: Privacy) -> Void

	/// Registered log handlers. The key is the UUID generated in `registerHandler()`
	static private var handlers: [String: (OSLogType, LogHandler)] = [:]

	/// Optional prefix to add (followed by a space) before all log statements. This facilitates easy filtering in logs/console output.
	static public var prefix: String?

	/**
	Register a log handler to be called whenever a log message is written.

	- parameter level: Lowest interesting log level
	- parameter handler: A log handler of the type `(String) -> ()` which will be called when a log level of the specified log level (or higher) is logged
	- returns: A `String` identifier for the log handler. Use this is you want to remove the handler using `unregisterHandler()`
	*/
	@discardableResult public static func registerHandler( level: OSLogType, handler: @escaping LogHandler ) -> String {
		let identifier = UUID().uuidString
		handlers[ identifier ] = (level, handler)

		return identifier
	}

	/**
	Access to the individual categories' `Logger` obhjects.

	Note that messages logged directly using a `Logger` are _not_ passed to registered log handlers.

	## Example:
	```
	Log.loggers[ .database ].error( "Failed to open database for user with email \(email, privacy: .sensitive)!" )
	```
	*/
	static let loggers = Loggers()

	/**
	Unregister (remove) a log handler.

	If no handler with the specified identifier is found, nothing happens.

	- parameter identifier: String identifier returned from `registerHandler()`
	*/
	public static func unregisterHandler( identifier: String ) {
		handlers.removeValue(forKey: identifier)
	}

	/**
	Unregister (remove) all log handlers.
	*/
	public static func unregisterAllHandlers() {
		handlers.removeAll()
	}

	/**
	Log a message and include file/function/line information.

	**NB:** It _is_ possible to specify explicit values for the `file`, `function` and `line` arguments, but leaving them unspecified will use the default values from the calling location.

	- parameter message: String message to log. Defaults to empty string so it is possible to simple call `Log.trace()` to log the current file/function/line number
	- parameter category: `LogCategory` for the message. Defaults to `LogCategory.default`.
	- parameter level: Log level. Default to `.debug`
	- parameter file: File path of calling file – _leave at default value of_ `#file`
	- parameter function: Calling function name – _leave at default value of_ `#function`
	- parameter line: Line number in calling file – _leave at default value of_ `#line`
	*/
	public static func trace( _ message: String = "", category: LogCategory = .default, level: OSLogType = .default, privacy: Log.Privacy = .public, file: String = #file, function: String = #function, line: Int = #line ) {
		let logMessage = "<\(file) in \(function), line \(line)> \(message)"
		let prefix = (self.prefix != nil ? self.prefix! + " " : "")	// OK to explicitly unwrap since it is checked for != nil

		switch privacy {
		case .public:
			category.logger.log(level: level, "\(prefix, privacy: .public)\(logMessage, privacy: .public)")

		case .private:
			category.logger.log(level: level, "\(prefix, privacy: .public)\(logMessage, privacy: .private( mask: .hash))" )
		}

		// Pass message on to all registered handlers with matching log level
		handlers.values.filter { level.rawValue >= $0.0.rawValue }.forEach { handler in
			handler.1( logMessage, category.identifier, level, privacy )
		}

	}

	/// Array of excluded (suppressed) categories whose log statements will _not_ be output.
	private static var excludedCategories: [LogCategory] = []

	/**
	 Exclude (suppress) the specified log categories from logging anything.

	 - parameter categories: Array of log categories to exclude (suppress)
	 */
	public static func excludeCategories(_ categories: [LogCategory]) {
		excludedCategories.append(contentsOf: categories)
	}

	/**
	 Re-include previously excluded log categories.

	 If any of the specified categories are _not_ currently excluded, nothing happens for that category.

	 - parameter categories: Array of log categories to re-include.
	 */
	public static func includeCategories(_ categories: [LogCategory]) {
		// Filter by keeping only those _not_ present in the specified categories
		let filteredCategories = excludedCategories.filter { category in
			!categories.contains(category)
		}
		excludedCategories = filteredCategories
	}
	/**
	Log a message.

	- parameter message: String message to log
	- parameter category: `LogCategory` for the message. Defaults to `LogCategory.default`.
	- parameter level: Log level. Defaults to `.default`
	*/
	public static func log( _ message: String, category: LogCategory = .default, level: OSLogType = .default, privacy: Log.Privacy = .public ) {
		// If category is excluded, return immediately
		if excludedCategories.contains(category) {
			return
		}

		let prefix = (self.prefix != nil ? self.prefix! + " " : "")	// OK to explicitly unwrap since it is checked for != nil

		switch privacy {
		case .public:
			category.logger.log(level: level, "\(prefix, privacy: .public)\(message, privacy: .public)")

		case .private:
			category.logger.log(level: level, "\(prefix, privacy: .public)\(message, privacy: .private( mask: .hash))" )
		}

		// Pass message on to all registered handlers with matching log level
		handlers.values.filter { level.rawValue >= $0.0.rawValue }.forEach { handler in
			handler.1( message, category.identifier, level, privacy )
		}
	}

	/**
	Log a message with log level `.info`

	- parameter message: String message to log
	- parameter category: `LogCategory` for the message. Defaults to `LogCategory.default`.
	*/
	public static func info( _ message: String, category: LogCategory = .default, privacy: Log.Privacy = .public ) {
		self.log(message, category: category, level: .info, privacy: privacy)
	}

	/**
	Log a message with log level `.debug`

	- parameter message: String message to log
	- parameter category: `LogCategory` for the message. Defaults to `LogCategory.default`.
	*/
	public static func debug( _ message: String, category: LogCategory = .default, privacy: Log.Privacy = .public ) {
		self.log(message, category: category, level: .debug, privacy: privacy)
	}

	/**
	Log a message with log level `.error`

	- parameter message: String message to log
	- parameter category: `LogCategory` for the message. Defaults to `LogCategory.default`.
	*/
	public static func error( _ message: String, category: LogCategory = .default, privacy: Log.Privacy = .public ) {
		self.log(message, category: category, level: .error, privacy: privacy)
	}

	/**
	Log a message with log level `.fault`

	- parameter message: String message to log
	- parameter category: `LogCategory` for the message. Defaults to `LogCategory.default`.
	*/
	public static func fault( _ message: String, category: LogCategory = .default, privacy: Log.Privacy = .public ) {
		self.log(message, category: category, level: .fault, privacy: privacy)
	}
}

/// Empty struct with a subscript access to log categories' `Logger` object
public struct Loggers {
	public subscript( category: LogCategory ) -> Logger {
		return category.logger
	}
}

/// Extension on OSLogType (level) to facilitate easier print()
extension OSLogType: CustomStringConvertible {
	public var description: String {
		switch self {
		case .debug:
			return "Debug"

		case .`default`:
			return "Default"

		case .info:
			return "Info"

		case .error:
			return "Error"

		case .fault:
			return "Fault"

		default:
			return "Other"
		}
	}
}
